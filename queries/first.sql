-- vnější spojení tabulek (LEFT JOIN)
-- select all element and games where it used.

SELECT element.name, type, color, size, gameset.name FROM element
LEFT JOIN gameset_element ON gameset_element.id_element = element.id_element
INNER JOIN gameset ON gameset.id_gameset = gameset_element.id_gameset;

--vnitřní spojení tabulek (INNER JOIN)
-- select from authors and gamesets only inner joined columns
SELECT author.name, gameset.name FROM author
INNER JOIN gameset ON gameset.id_author = author.id_author;


--podmínku na data (WHERE)
-- select all white elements from elements
SELECT element.name, element.color FROM element
WHERE element.color = 'white';

--agregaci a podmínku na hodnotu agregační funkce (AVG())
--vnořený SELECT ()
-- select all games where max_players is greater than average max_player

SELECT gameset.name, boardgame.max_players FROM boardgame
INNER JOIN gameset ON boardgame.id_gameset = gameset.id_gameset
WHERE boardgame.max_players >
      (SELECT AVG(boardgame.max_players) FROM boardgame);



--řazení a stránkování (ASC PAGINATION)
-- select from all elements ordered by type name desc five from 6 to 10

SELECT * FROM element ORDER BY type DESC LIMIT 5 OFFSET 5;

--množinové operace ()
-- select all games names from game+gameset and connect it with all expansion names from expansion+gameset

SELECT gameset.name from boardgame
                            INNER JOIN gameset ON gameset.id_gameset = boardgame.id_gameset
UNION ALL
SELECT gameset.name from expansion
                                     INNER JOIN gameset ON gameset.id_gameset = expansion.id_gameset;





-- esle
SELECT gameset.name from gameset
INNER JOIN author on author.id_author = gameset.id_author
RIGHT JOIN gameset_element on gameset.id_gameset = gameset_element.id_gameset
INNER JOIN element on element.id_element = gameset_element.id_element
WHERE author.id_author = 1 AND element.type = 'card'

--vnořený SELECT ()

