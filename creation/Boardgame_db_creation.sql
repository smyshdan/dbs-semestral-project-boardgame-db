-- CREATE ENUMS
DROP TYPE IF EXISTS colors CASCADE;
CREATE TYPE colors AS ENUM ('black', 'white', 'red', 'orange', 'yellow', 'blue', 'green', 'purple', 'gray', 'mixed', 'other', 'unknown');
DROP TYPE IF EXISTS types_of_elements CASCADE;
CREATE TYPE types_of_elements AS ENUM ('card', 'meeple', 'hourglass', 'figure', 'chip', 'board', 'box', 'scoreboard',
    'coin', 'dice', 'tile', 'stopwatch', 'timer', 'gamemaster screen', 'money', 'pawn', 'rules', 'manual', 'other', 'unknown');

-- AUTHOR TABLE

DROP TABLE IF EXISTS Author CASCADE;

CREATE TABLE IF NOT EXISTS Author
(
    id_author serial       NOT NULL,
    name      varchar(100) NOT NULL,
    birthday  date         NOT NULL,

    PRIMARY KEY (id_author),
    CONSTRAINT "unique_name-birthday" UNIQUE (name, birthday),
    CONSTRAINT "check_birthday" CHECK (birthday < CURRENT_DATE),
    CONSTRAINT "check_name_not_empty" CHECK ( LENGTH(TRIM(name)) > 0 )
);

-- GAMESET TABLE

DROP TABLE IF EXISTS Gameset CASCADE;

CREATE TABLE IF NOT EXISTS Gameset
(
    id_gameset   serial       NOT NULL,
    id_author    int          NOT NULL DEFAULT 1,

    name         varchar(100) NOT NULL,
    release_date date         NOT NULL DEFAULT CURRENT_DATE,
    rules        text         NOT NULL,

    PRIMARY KEY (id_gameset),
    CONSTRAINT "fk_author" FOREIGN KEY (id_author) REFERENCES Author
        ON UPDATE CASCADE
        ON DELETE SET DEFAULT,
    CONSTRAINT "unique_name-author_id" UNIQUE (name, id_author),
    CONSTRAINT "check_name_not_empty" CHECK ( LENGTH(TRIM(name)) > 0 ),
    CONSTRAINT "check_rules_not_empty" CHECK ( LENGTH(TRIM(rules)) > 0 )
);


-- BOARDGAME TABLE

DROP TABLE IF EXISTS Boardgame CASCADE;

CREATE TABLE IF NOT EXISTS Boardgame
(
    id_boardgame serial NOT NULL,
    id_gameset   int    NOT NULL,

    max_players  int    NOT NULL,
    min_players  int    NOT NULL DEFAULT 1,

    PRIMARY KEY (id_boardgame),
    CONSTRAINT "fk_gameset" FOREIGN KEY (id_gameset) REFERENCES Gameset
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT "unique_gameset" UNIQUE (id_gameset),
    CONSTRAINT "check_min_players" CHECK ( min_players > 0 ),
    CONSTRAINT "check_max_players" CHECK ( max_players > 0 )
);

-- CONTINUATION_OF TABLE

DROP TABLE IF EXISTS Continuation_of CASCADE;

CREATE TABLE IF NOT EXISTS Continuation_of
(
    id_first_boardgame        int NOT NULL,
    id_continuation_boardgame int NOT NULL,

    CONSTRAINT "fk_first_boardgame" FOREIGN KEY (id_first_boardgame) REFERENCES Boardgame
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT "fk_continuation_boardgame" FOREIGN KEY (id_continuation_boardgame) REFERENCES Boardgame
        ON UPDATE CASCADE
        ON DELETE CASCADE,

    CONSTRAINT "unique_first_boardgame-continuation_boardgame" UNIQUE (id_first_boardgame, id_continuation_boardgame)
);

-- ELEMENT TABLE

DROP TABLE IF EXISTS Element CASCADE;

CREATE TABLE IF NOT EXISTS Element
(
    id_element  serial            NOT NULL,
    name        varchar(100)      NOT NULL,
    type        types_of_elements NOT NULL DEFAULT 'unknown',
    color       colors            NOT NULL DEFAULT 'unknown',
    size        varchar(100)      NOT NULL DEFAULT 'unknown',
    description text              NOT NULL DEFAULT 'empty description',


    PRIMARY KEY (id_element),
    CONSTRAINT "unique_name-type-color-size-description" UNIQUE (name, type, color, size, description),
    CONSTRAINT "check_name_not_empty" CHECK ( LENGTH(TRIM(name)) > 0 ),
    CONSTRAINT "check_size_not_empty" CHECK ( LENGTH(TRIM(size)) > 0 ),
    CONSTRAINT "check_description_not_empty" CHECK ( LENGTH(TRIM(description)) > 0 )
);

-- GAMESET_ELEMENT TABLE

DROP TABLE IF EXISTS GAMESET_ELEMENT CASCADE;

CREATE TABLE IF NOT EXISTS GAMESET_ELEMENT
(
    id_gameset int NOT NULL,
    id_element int NOT NULL,
    amount     int NOT NULL DEFAULT 1,

    CONSTRAINT "unique_gameset-element" UNIQUE (id_gameset, id_element),
    CONSTRAINT "fk_gameset" FOREIGN KEY (id_gameset) REFERENCES Gameset
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT "fk_element" FOREIGN KEY (id_element) REFERENCES Element
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT "check_amount" CHECK ( amount > 0 )
);

-- SERIES TABLE

DROP TABLE IF EXISTS Series CASCADE;

CREATE TABLE IF NOT EXISTS Series
(
    id_series serial       NOT NULL,
    name      varchar(100) NOT NULL,

    PRIMARY KEY (id_series),
    CONSTRAINT "unique_name" UNIQUE (name),
    CONSTRAINT "check_name_not_empty" CHECK ( LENGTH(TRIM(name)) > 0 )
);

-- EXPANSION TABLE

DROP TABLE IF EXISTS Expansion CASCADE;

CREATE TABLE IF NOT EXISTS Expansion
(
    id_expansion serial NOT NULL,
    id_gameset   int    NOT NULL,
    id_series    int    NOT NULL,

    PRIMARY KEY (id_expansion),
    CONSTRAINT "fk_gameset" FOREIGN KEY (id_gameset) REFERENCES Gameset
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT "expansion_of_series" FOREIGN KEY (id_series) REFERENCES Series
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT "unique_gameset-series" UNIQUE (id_gameset, id_series)
);


-- BOARDGAME_OF_SERIES TABLE

DROP TABLE IF EXISTS Boardgame_of_Series CASCADE;

CREATE TABLE IF NOT EXISTS Boardgame_of_Series
(
    id_boardgame int NOT NULL,
    id_series    int NOT NULL,

    CONSTRAINT "fk_boardgame" FOREIGN KEY (id_boardgame) REFERENCES Boardgame
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT "fk_series" FOREIGN KEY (id_series) REFERENCES Series
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT "unique_boardgame-series" UNIQUE (id_boardgame, id_series)
);

