-- MUNCHKIN
INSERT INTO gameset(name, release_date, id_author, rules)
VALUES ('Monopoly', '1935-2-6', 2, 'Monopoly Rules
Objective: The objective of Monopoly is to become the wealthiest player by buying, renting, and trading properties and bankrupting opponents.

Setup:

Lay out the game board and place all property deeds, Chance and Community Chest cards, houses, and hotels near the board.
Each player selects a token and places it on the "Go" space. Choose a banker to handle all transactions and money.
Gameplay:

Players take turns rolling the dice and moving their token around the board in a clockwise direction.
When a player lands on an unowned property, they have the option to purchase it from the bank. If they decline, the property is auctioned to the highest bidder among the players.
If a player lands on a property owned by another player, they must pay rent to the owner. Rent amounts vary depending on the property''s value and whether it has houses or hotels built on it.
Properties can be improved by purchasing and building houses and hotels. Each property group must be developed evenly, meaning all properties of a color group must have the same number of houses before any can be upgraded to hotels.
Players may also land on special spaces such as Chance, Community Chest, Jail, Free Parking, and Go to Jail, each with its own set of instructions and consequences.
Jail can be entered either by landing on the "Go to Jail" space, drawing a Chance or Community Chest card that directs the player to "Go to Jail," or rolling doubles three times in a row. While in Jail, a player can still collect rent, buy and sell properties, and participate in auctions.
The game continues until all players except one have gone bankrupt. The last remaining player is declared the winner.
Winning the Game:
The winner of Monopoly is the last player remaining after all others have gone bankrupt.

Variant Rules:

There are many variant rules and house rules that players may agree to before starting the game, such as "Free Parking Jackpot" where money collected from various penalties is placed in the center of the board and awarded to the player who lands on Free Parking.
');


-- MUNCHKIN

INSERT INTO gameset(name, release_date, id_author, rules)
VALUES ('Munchkin', '2001-05-18', 3, 'Summary of "Munchkin" Rules
Objective: The objective of "Munchkin" is to be the first player to reach level 10 by defeating monsters, obtaining treasures, and using various cards to enhance your character.

Setup:

Each player starts at level 1 with no items.
Shuffle the Door cards and Treasure cards into separate decks and place them face down in the play area.
Each player draws a hand of cards (usually four or five) from the Door deck.
Gameplay:

On a player''s turn, they may "Kick Down the Door" by drawing a Door card. If it''s a monster, they must fight it; if it''s a curse, they resolve its effect; if it''s a class, race, or other modifier, they can play it immediately or keep it for later.
Players may also "Look for Trouble" by playing a monster from their hand to fight. If they defeat the monster, they gain levels and treasures.
Alternatively, players may "Loot the Room" by drawing another Door card without fighting a monster.
Players may play cards from their hand at any time during their turn to modify their character, affect other players, or alter the outcome of combat.
Combat is resolved by comparing the player''s level and bonuses to the monster''s level and modifiers. Players may ask for help from other players or use various cards to enhance their chances of winning.
If a player defeats a monster, they go up a level and draw a number of Treasure cards as indicated by the monster''s treasure value.
The game continues clockwise with each player taking turns until one player reaches level 10.
Winning the Game:
The first player to reach level 10 by defeating monsters and acquiring treasures is declared the winner.

Variant Rules:

There are numerous variant rules, card combinations, and expansions that add complexity and strategic depth to the game. Players may agree upon house rules or use specific expansion sets to customize their gameplay experience.
Additional Notes:

"Munchkin" is known for its humorous tone and encourages players to engage in social interaction, negotiation, and backstabbing as they navigate the game''s challenges.
Players should consult the full rulebook for detailed explanations of card types, interactions, and special rules.
This summary provides a basic overview of the gameplay mechanics and objectives of "Munchkin." For a comprehensive understanding of the game, it''s recommended to read the full rulebook provided with the game or available online.');

-- MUNCHKIN 2: Unnatural Axe

INSERT INTO gameset(name, release_date, id_author, rules)
VALUES ('Munchkin 2: Unnatural Axe', '2002-09-14', 3, 'Setup: "Munchkin 2: Unnatural Axe" is played in conjunction with the base game of "Munchkin." Shuffle the Door cards and Treasure cards from the expansion into their respective decks from the base game.
Gameplay: During the game, players continue to explore dungeons, fight monsters, and collect treasures as they attempt to reach level 10 and win the game. The expansion introduces new Door and Treasure cards, which may feature additional monsters, items, or mechanics to enhance gameplay.
New Mechanics: The expansion may introduce new mechanics, such as special abilities on monsters, new classes or races for players to choose from, or new types of treasures with unique effects.
Integration: The cards from "Munchkin 2: Unnatural Axe" can be shuffled into the existing decks from the base game, expanding the variety of encounters and rewards available to players during gameplay.
Victory: The first player to reach level 10 by defeating monsters and collecting treasures, using cards from both the base game and the expansion, wins the game.');

-- UNO

INSERT INTO gameset(name, release_date, id_author, rules)
VALUES ('UNO', '1971-01-01', 1, 'UNO is a shedding-type card game where players match cards in their hand by color or number to the top card of the discard pile. Action cards introduce special moves such as skips, reverses, and draws.');

-- POKER

INSERT INTO gameset(name, release_date, id_author, rules)
VALUES ('POKER', '1000-10-10', 1, 'Poker is a family of gambling card games that involve betting and individual play. The goal is to have the best hand or to bluff other players into folding. Variants include Texas Hold''em, Omaha, and Seven-Card Stud.');

-- TIC TAC TOE

INSERT INTO gameset(name, release_date, id_author, rules)
VALUES ('Tic-Tac-Toe', '1000-10-10', 1, 'A simple game played on a 3x3 grid where players take turns placing their symbol (X or O) with the objective of getting three in a row horizontally, vertically, or diagonally.');



