-- BASE FOR ALL

INSERT INTO element(name, type, color, size, description)
VALUES ('Six-sided dice',
        'dice',
        'mixed',
        '1 cm x 1 cm',
        'Dice used for determining the number of spaces a player moves during their turn and in many other cases.');

INSERT INTO element(name, type, color, size, description)
VALUES ('Playing card',
        'card',
        'mixed',
        '6 cm x 9 cm',
        'Playing cards are versatile and can be used for a wide range of games, including poker, bridge, rummy, solitaire, and many others. They are an essential component of many traditional and modern card games, providing entertainment and enjoyment for players of all ages.');

INSERT INTO element(name, type, color, size, description)
VALUES ('Chip',
        'chip',
        'mixed',
        '3 cm x 3 cm',
        'Poker chips are specialized tokens used in various gambling and card games, most notably in poker, to represent monetary value or bets. Here''s a description of poker chips.');

INSERT INTO element(name, type, color, size, description)
VALUES ('Pencil',
        'other',
        'mixed',
        '15 cm x 0.5 cm',
        'A pencil is a writing instrument typically made of a slender cylindrical shaft of wood, graphite, or a similar substance.');



-- MONOPOLY

INSERT INTO element(name, type, color, size, description)
VALUES ('Banknotes',
        'money',
        'green',
        '15 cm x 7 cm',
        'Banknotes comes in different denominations and is used for purchasing properties,' ||
        ' paying rent, and various other transactions throughout the game. The money bills measure approximately' ||
        ' 15 cm x 7 cm and are typically colored in various shades of green.');
INSERT INTO element(name, type, color, size, description)
VALUES ('Monopoly game board',
        'board',
        'mixed',
        '50 cm x 50 cm',
        'The iconic Monopoly game board measures approximately 50 cm x 50 cm and features a square grid representing' ||
        ' various properties. It typically has a colorful design with vibrant property squares and other spaces.');
INSERT INTO element(name, type, color, size, description)
VALUES ('Pawn',
        'figure',
        'mixed',
        '2 cm x 2 cm x 2 cm',
        'Players select a token to represent themselves on the game board. Common tokens include the thimble, top ' ||
        'hat, racecar, boot, wheelbarrow, battleship, cat, dog, and others, depending on the edition.');
INSERT INTO element(name, type, color, size, description)
VALUES ('Property cards',
        'card',
        'white',
        '7 cm x 5 cm',
        'Each property on the game board corresponds to a property card, which includes information such as the propertys ' ||
        'name, purchase price, rent prices, mortgage value, and building costs. Property cards are rectangular and color-coded to match the properties on the board.');

INSERT INTO element(name, type, color, size, description)
VALUES ('Chance cards',
        'card',
        'yellow',
        '9 cm x 6 cm',
        'These cards feature various instructions or events that players must follow when drawn. They can result in positive or negative outcomes for the player. ');

INSERT INTO element(name, type, color, size, description)
VALUES ('Community chest cards',
        'card',
        'orange',
        '9 cm x 6 cm',
        'Similar to Chance cards, Community Chest cards also feature instructions or events that players must follow when drawn. The outcomes can vary widely.');

INSERT INTO element(name, type, color, size, description)
VALUES ('Houses and Hotels',
        'figure',
        'red',
        '2 cm x 2 cm x 1 cm',
        'Players can purchase houses and hotels to improve their properties, increasing the rent that opponents must pay when they land on them.');

INSERT INTO element(name, type, color, size, description)
VALUES ('Six-sided dice',
        'dice',
        'white',
        '1.5 cm x 1.5 cm x 1.5 cm',
        'Dice used for determining the number of spaces a player moves during their turn and in many other cases.');

INSERT INTO element(name, type, color, size, description)
VALUES ('Banker''s Tray',
        'box',
        'gray',
        '25 cm x 20 cm x 5 cm',
        'This tray holds the money, property cards, Chance cards, Community Chest cards, and other components during gameplay.' ||
        ' The size of the bankers tray can vary depending on the edition, but it typically measures approximately');

-- MUNCHKIN

INSERT INTO element(name, type, color, size, description)
VALUES ('Door cards',
        'card',
        'white',
        '6.35 x 8.89 cm',
        'Door cards represent various challenges, encounters, or events that players face as they explore dungeons, ' ||
        'encounter monsters, or interact with other players. These cards often include monsters to fight, curses to overcome, or beneficial items to acquire.');

INSERT INTO element(name, type, color, size, description)
VALUES ('Treasure cards',
        'card',
        'orange',
        '6.35 x 8.89 cm',
        'Treasure cards represent the rewards and items that players collect as they progress through the game. ' ||
        'These cards typically include weapons, armor, magical items, or other beneficial effects that help players overcome challenges and increase their power.');

INSERT INTO element(name, type, color, size, description)
VALUES ('Rulebook',
        'rules',
        'white',
        '20 cm x 30 cm',
        'The rulebook provides instructions on how to play the game, including setup, gameplay mechanics, and winning conditions.');

INSERT INTO element(name, type, color, size, description)
VALUES ('Dice',
        'dice',
        'blue',
        '1 cm x 1 cm',
        'Six-sided dice used for resolving combat and other actions in the game. Players may also use additional custom dice provided in expansions.');

-- SOMETHING ELSE
