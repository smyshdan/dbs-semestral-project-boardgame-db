-- MONOPOLY
INSERT INTO gameset_element(id_gameset, id_element, amount) VALUES (2, 1, 200);
INSERT INTO gameset_element(id_gameset, id_element, amount) VALUES (2, 2, 1);
INSERT INTO gameset_element(id_gameset, id_element, amount) VALUES (2, 3, 12);
INSERT INTO gameset_element(id_gameset, id_element, amount) VALUES (2, 4, 32);
INSERT INTO gameset_element(id_gameset, id_element, amount) VALUES (2, 5, 16);
INSERT INTO gameset_element(id_gameset, id_element, amount) VALUES (2, 6, 16);
INSERT INTO gameset_element(id_gameset, id_element, amount) VALUES (2, 7, 44);
INSERT INTO gameset_element(id_gameset, id_element, amount) VALUES (2, 8, 2);
INSERT INTO gameset_element(id_gameset, id_element, amount) VALUES (2, 9, 1);
-- MUNCHKIN
INSERT INTO gameset_element(id_gameset, id_element, amount) VALUES (3, 10, 100);
INSERT INTO gameset_element(id_gameset, id_element, amount) VALUES (3, 11, 80);
INSERT INTO gameset_element(id_gameset, id_element, amount) VALUES (3, 12, 1);
INSERT INTO gameset_element(id_gameset, id_element, amount) VALUES (3, 13, 1);

-- ELSE
INSERT INTO gameset_element(id_gameset, id_element, amount) VALUES (4, 15, 100);
INSERT INTO gameset_element(id_gameset, id_element, amount) VALUES (5, 15, 52);
INSERT INTO gameset_element(id_gameset, id_element, amount) VALUES (6, 17, 1);

